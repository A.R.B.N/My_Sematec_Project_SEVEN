package ir.arbn.www.mysematecprojectseven;

/**
 * Created by A.R.B.N on 2/9/2018.
 */

public class PublicMethods {
    public static String convertFtoC(String fStr) {
        double f = Float.parseFloat(fStr);
        double c = (f - 32) / 1.8;
        return (int) c + "°C";
    }
}
