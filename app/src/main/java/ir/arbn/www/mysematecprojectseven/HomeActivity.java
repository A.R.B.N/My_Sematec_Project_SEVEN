package ir.arbn.www.mysematecprojectseven;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.List;

import cz.msebera.android.httpclient.Header;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    EditText City;
    TextView Result;
    ProgressDialog ProgressDialog;
    ListView MyList;
    ImageView ResultImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        City = findViewById(R.id.City);
        Result = findViewById(R.id.Result);
        ResultImage = findViewById(R.id.ResultImage);
        findViewById(R.id.Search).setOnClickListener(this);
        MyList = findViewById(R.id.MyList);
        ProgressDialog = new ProgressDialog(this);
        ProgressDialog.setTitle("Loading");
        ProgressDialog.setMessage("Please Wait...");
    }

    @Override
    public void onClick(View view) {
        String cityVal = City.getText().toString();
        getDataFromYahoo(cityVal);
    }

    private void getDataFromYahoo(String cityVal) {
        ProgressDialog.show();
        String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" + cityVal + "%2C%20IR%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(HomeActivity.this, "Error In Connection!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseData(responseString);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                ProgressDialog.dismiss();
            }
        });
    }

    private void parseData(String responseString) {
        Gson Gson = new Gson();
        try {
            YahooWeather yahoo = Gson.fromJson(responseString, YahooWeather.class);
            String resultStr1 = yahoo.getQuery().getResults().getChannel().getItem().getCondition().getTemp();
            Result.setText("Temperature of " + City.getText().toString().toUpperCase() + ": " + " " + PublicMethods.convertFtoC(resultStr1));
            String resultStr2 = yahoo.getQuery().getResults().getChannel().getItem().getCondition().getText();
            if (resultStr2.toLowerCase().contains("sun"))
                Glide.with(ResultImage).load(R.drawable.sun).into(ResultImage);
            if (resultStr2.toLowerCase().contains("clear"))
                Glide.with(ResultImage).load(R.drawable.night).into(ResultImage);
            if (resultStr2.toLowerCase().contains("cloud"))
                Glide.with(ResultImage).load(R.drawable.cloudy).into(ResultImage);
            if (resultStr2.toLowerCase().contains("shower"))
                Glide.with(ResultImage).load(R.drawable.rain).into(ResultImage);
            if (resultStr2.toLowerCase().contains("rain"))
                Glide.with(ResultImage).load(R.drawable.rain).into(ResultImage);
            if (resultStr2.toLowerCase().contains("snow"))
                Glide.with(ResultImage).load(R.drawable.snowy).into(ResultImage);
            if (resultStr2.toLowerCase().contains("wind"))
                Glide.with(ResultImage).load(R.drawable.windy).into(ResultImage);
            if (resultStr2.toLowerCase().contains("breezy"))
                Glide.with(ResultImage).load(R.drawable.windy).into(ResultImage);
            if (resultStr2.toLowerCase().contains("storm"))
                Glide.with(ResultImage).load(R.drawable.storm).into(ResultImage);
            saveForecastToDB(City.getText().toString(), yahoo.getQuery().getResults().getChannel().getItem().getForecast());
//Forcast List
            MyListAdapter Adapter = new MyListAdapter(this, yahoo.getQuery().getResults().getChannel().getItem().getForecast());
            MyList.setAdapter(Adapter);
        } catch (Exception e) {
            Toast.makeText(this, "City Not Found!", Toast.LENGTH_SHORT).show();
            Result.setText("The City You Searched For Couldn't Be Found!");
            MyList.setAdapter(null);
            ResultImage.setImageDrawable(null);
        }
    }

    private void saveForecastToDB(String city, List<Forecast> forecasts) {
        for ( Forecast fc : forecasts ) {
            ForecastForDBModel fcdb= new ForecastForDBModel();
            fcdb.setCity(city);
            fcdb.setCode(fc.getCode());
            fcdb.setDate(fc.getDate());
            fcdb.setDay(fc.getDay());
            fcdb.setHigh(fc.getHigh());
            fcdb.setLow(fc.getLow());
            fcdb.setText(fc.getText());
            fcdb.save();
        }
    }
}