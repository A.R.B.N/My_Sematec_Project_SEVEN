package ir.arbn.www.mysematecprojectseven;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by A.R.B.N on 2/9/2018.
 */

public class MyListAdapter extends BaseAdapter {
    Context mContext;
    List<Forecast> forecasts;

    public MyListAdapter(Context mContext, List<Forecast> forecasts) {
        this.mContext = mContext;
        this.forecasts = forecasts;
    }

    @Override
    public int getCount() {
        return forecasts.size();
    }

    @Override
    public Object getItem(int position) {
        return forecasts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_list_mylist, null);
        TextView Date = v.findViewById(R.id.Date);
        TextView Day = v.findViewById(R.id.Day);
        TextView High = v.findViewById(R.id.High);
        TextView Low = v.findViewById(R.id.Low);
        TextView Text = v.findViewById(R.id.Text);
        ImageView Img = v.findViewById(R.id.Img);
        Forecast ThisVal = forecasts.get(position);
        Date.setText("Date: " + ThisVal.getDate());
        Day.setText("Day: " + ThisVal.getDay());
        High.setText("Highest Temperature: " + PublicMethods.convertFtoC(ThisVal.getHigh()));
        Low.setText("Lowest Temperature: " + PublicMethods.convertFtoC(ThisVal.getLow()));
        Text.setText("Forecast: " + ThisVal.getText());
        if (ThisVal.getText().toLowerCase().contains("sun"))
            Glide.with(mContext).load(R.drawable.sun).into(Img);
        if (ThisVal.getText().toLowerCase().contains("cloud"))
            Glide.with(mContext).load(R.drawable.cloudy).into(Img);
        if (ThisVal.getText().toLowerCase().contains("shower"))
            Glide.with(mContext).load(R.drawable.rain).into(Img);
        if (ThisVal.getText().toLowerCase().contains("rain"))
            Glide.with(mContext).load(R.drawable.rain).into(Img);
        if (ThisVal.getText().toLowerCase().contains("snow"))
            Glide.with(mContext).load(R.drawable.snowy).into(Img);
        if (ThisVal.getText().toLowerCase().contains("wind"))
            Glide.with(mContext).load(R.drawable.windy).into(Img);
        if (ThisVal.getText().toLowerCase().contains("breezy"))
            Glide.with(mContext).load(R.drawable.windy).into(Img);
        if (ThisVal.getText().toLowerCase().contains("storm"))
            Glide.with(mContext).load(R.drawable.storm).into(Img);
        return v;
    }
}
